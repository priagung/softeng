package Initialize;

import Game.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

public class Initialize {

    private int n;
    private Available available;
    private ArrayList<String> gameListTitle = new ArrayList<String>();
    private ArrayList<String> bundleListTitle = new ArrayList<String>();

    public Initialize() {
        this.available = new Available();
    }


    public Available initialize() throws IOException {
        URL path = Initialize.class.getResource("input.txt");
        File file = new File(path.getFile());
        BufferedReader br = new BufferedReader(new FileReader(file));

        String input = br.readLine();
        String[] inputNoSpace = input.split(" ");
        n = Integer.parseInt(inputNoSpace[0]);
        for (int i = 0; i < n; i++) {
            //System.out.print(n);
            String gameInput = br.readLine();
            String[] gameCleaned = gameInput.split(" ");
            Game tempGame = new Game(gameCleaned[0], Integer.parseInt(gameCleaned[1]));
            //System.out.println(tempGame.getTitle());
            //System.out.println(gameListTitle);
            if(!gameListTitle.contains(tempGame.getTitle())) {
                gameListTitle.add(tempGame.getTitle());
                for (int j = 3; j < (Integer.parseInt(gameCleaned[2])+3); j++) {
                    //System.out.print("3");
                    tempGame.addGenre(gameCleaned[j]);
                    available.addGenreAv(gameCleaned[j]);
                }
                available.addGameAv(tempGame);
                //System.out.println(available.getGenreListAv());
            } else{ throw new IOException("Duplicate Game Title Detected"); }
        }


        String inputBundle = br.readLine();
        String[] inputBundleNoSpace = inputBundle.split(" ");
        n = Integer.parseInt(inputBundleNoSpace[0]);


        for (int i = 0; i < n; i++) {
            String bundleInput = br.readLine();
            String[] bundleCleaned = bundleInput.split(" ");
            Bundle tempBundle = new Bundle(bundleCleaned[0], Integer.parseInt(bundleCleaned[1]));

            if(!bundleListTitle.contains(tempBundle.getBundleTitle())) {
                bundleListTitle.add(tempBundle.getBundleTitle());
                //System.out.println("test");


                for (int j = 0; j < Integer.parseInt(bundleCleaned[2]); j++) {
                    //System.out.println(j);
                    for (int k = 0; k < gameListTitle.size(); k++) {
                        //System.out.println(k);
                        //System.out.println(gameListTitle.get(k));
                        //System.out.println(available.getGameListAv().get(k));
                        if (gameListTitle.get(k).equals(bundleCleaned[j+3]))
                            //System.out.println(gameListTitle.get(k));
                            tempBundle.addGame(available.getGameListAv().get(k));
                        available.addBundleAv(tempBundle);
                    }
                }
            }else{ throw new IOException("Duplicate Bundle Title Detected"); }
        }
        br.close();
        return available;
    }
}


