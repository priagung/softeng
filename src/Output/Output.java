package Output;

import Game.*;

import java.util.ArrayList;

public class Output {

    private ArrayList<Game> gameGenre = new ArrayList<>();
    private ArrayList<Bundle> bundleGenre = new ArrayList<>();

    public Output(){ }

    public void printAvailableGame(Available available){
        for (int i=0; i<available.getGameListAv().size(); i++){
            System.out.println("    " + available.getGameListAv().get(i).getId() + " " + available.getGameListAv().get(i).getTitle() +
                    " Rp." + available.getGameListAv().get(i).getPrice());
        }
        System.out.println();
    }

    public void printAvailableBundle(Available available){
        for(int i=0; i<available.getBundleListAv().size(); i++){
            //System.out.println(available.getBundleListAv().size());
            System.out.println(available.getBundleListAv().get(i).getId() + " " + available.getBundleListAv().get(i).getBundleTitle() +
                    " Rp." + available.getBundleListAv().get(i).getBundlePrice());
            System.out.println("    "  +
                    available.getBundleListAv().get(i).getBundleTitle() + " Bundle includes: ");
            for(Game game : available.getBundleListAv().get(i).getGameList()){
                System.out.println("    " + game.getId() + " " + game.getTitle());
            }
            System.out.println("    This " + available.getBundleListAv().get(i).getBundleTitle() + " Bundle saves Rp." +
                    (available.getBundleListAv().get(i).getNormalPrice() -
                    available.getBundleListAv().get(i).getBundlePrice()));
            System.out.println();
            System.out.println();
        }
        //System.out.println();
    }

    public void printCart(Cart cart) {
        if (cart.getGameCart().size() > 0) {
            System.out.println("These are the game/s in your cart\n");
            int sameItemCounter = 1;
            for (int i = 0; i < cart.getGameCart().size(); i++) {
                //System.out.println(i);
                if(i == cart.getGameCart().size()-1){
                    System.out.println("    " + cart.getGameCart().get(i).getTitle() + "[" + sameItemCounter + " Item/s]");
                    sameItemCounter = 1;
                }
                else if(!cart.getGameCart().get(i).getTitle().equals(cart.getGameCart().get(i+1).getTitle())) {
                    System.out.println("    " +cart.getGameCart().get(i).getTitle() + "[" + sameItemCounter + " Item/s]");
                    sameItemCounter = 1;
                }
                else{
                    sameItemCounter++;
                }
            }
        }
        if (cart.getBundleCart().size() > 0) {
            System.out.println("\nAnd these bundle/s\n");
            int sameItemCounter = 1;
            for (int i = 0; i < cart.getBundleCart().size(); i++) {
                //System.out.println(i);
                if(i == cart.getBundleCart().size()-1){
                    System.out.println("    " +cart.getBundleCart().get(i).getBundleTitle() + "[" + sameItemCounter + " Item/s]");
                    sameItemCounter = 1;
                }
                else if(!cart.getBundleCart().get(i).getBundleTitle().equals(cart.getBundleCart().get(i+1).getBundleTitle())) {
                    System.out.println("    " +cart.getBundleCart().get(i).getBundleTitle() + "[" + sameItemCounter + " Item/s]");
                    sameItemCounter = 1;
                }
                else{
                    sameItemCounter++;
                }
            }
        }
        System.out.println("\nYour cart has " + cart.getItemAmount() + " Item/s");
        System.out.println("Total Price of item/s in your cart : Rp." + cart.getTotalPrice() + "\n");

    }

    public void printSearchItemByGenre(Available available,String genre) {
        for (int i = 0; i < available.getGameListAv().size(); i++) {
            if (available.getGameListAv().get(i).getGenre().contains(genre)) {
                // Genre was found, print an arraylist of gametitle that includes that genre
                gameGenre.add(available.getGameListAv().get(i));
            }
            // after getting through all the games to find that genre print the id, title, price
        }
        for (int i = 0; i < available.getBundleListAv().size(); i++){
            for(int j=0; j < available.getBundleListAv().get(i).getGameList().size(); j++){
                //game genre that included in a bundle was found
                //System.out.println(available.getBundleListAv().get(i).getGameList().get(j).getGenre());
                if(available.getBundleListAv().get(i).getGameList().get(j).getGenre().contains(genre)
                        && (!bundleGenre.contains(available.getBundleListAv().get(i)))){
                    bundleGenre.add(available.getBundleListAv().get(i));
                }
            }
        }
        System.out.println("Here are the item/s with your chosen genre:");
        for(Game game : gameGenre){
            System.out.print("  " + game.getId() + " " + game.getTitle() + " Rp." + game.getPrice() + "\n");
        }
        for(Bundle bundle : bundleGenre){
            System.out.print("  " +bundle.getId() + " " + bundle.getBundleTitle() + " Rp." + bundle.getBundlePrice() + "\n");
            System.out.println("    This bundle Includes: ");
            for(int i=0; i<bundle.getGameList().size() ;i++){
                System.out.println("    " + bundle.getGameList().get(i).getId()+ " " + bundle.getGameList().get(i).getTitle());
            }
        }
    }



}
