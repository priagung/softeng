package Main;

import Game.*;
import Initialize.*;
import Output.*;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.StringTokenizer;

public class Main {

    private static InputReader in;
    private static Initialize initialize = new Initialize();
    private static Output output = new Output();


    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);

        readInput();
    }

    private static void readInput() throws IOException {
        Available available = initialize.initialize();


        boolean still = true;
        //a simple and straightforward text for explaining the app
        System.out.println();
        System.out.println("============================GameCart===============================");
        System.out.println("Type 'commands' to see the whole command list for this application.");
        System.out.println();
        while(still){
            String input = in.next();

            switch(input) {
                case "commands":
                    //explain commands and what its purpose
                    System.out.println("\nHere are the available commands in GameCart!");
                    System.out.println("\n>available");
                    System.out.println("    View the registered item/s in the system.\n");
                    System.out.println(">search");
                    System.out.println("    Search an item/s by a genre you like!\n");
                    System.out.println(">order");
                    System.out.println("    Ordering items by putting item/s on your cart.\n");
                    System.out.println(">exit");
                    System.out.println("    Terminate the application\n");
                    break;

                case "order":
                    //customer choose what to order.
                    System.out.println("you can start putting game/s to your cart by doing:");
                    System.out.println("GameID Amount,GameID2 Amount,GameIDx Amount.");
                    System.out.println("for example: G3 3, G5 2, G1 1");
                    Cart currentCart = new Cart();
                    int itemsAvailable = 0;

                    String nextInput = in.nextLine();

                    //String orderInput = in.nextLine();
                    String[] orderArray = nextInput.split(",");
                    // Individual Game Order
                    for (int i = 0; i < orderArray.length; i++) {
                        String[] eachOrder = orderArray[i].split(" ");
                        // if ID starts with G indicates putting game to the cart.
                        String orderID = eachOrder[0];

                        if (orderID.startsWith("G")) {
                            int gameAmount = Integer.parseInt(eachOrder[1]);
                            for (int j = 0; j < available.getGameListAv().size(); j++) {
                                if (available.getGameListAv().get(j).getId().equals(orderID)) {
                                    itemsAvailable++;
                                    currentCart.addGameToCart(available.getGameListAv().get(j), gameAmount);
                                }
                            }
                        } else if (orderID.startsWith("B")) {
                            int bundleAmount = Integer.parseInt(eachOrder[1]);
                            for (int j = 0; j < available.getBundleListAv().size(); j++) {
                                if (available.getBundleListAv().get(j).getId().equals(orderID)) {
                                    itemsAvailable++;
                                    currentCart.addBundleToCart(available.getBundleListAv().get(j), bundleAmount);
                                }
                            }
                        }
                    }
                    // if all of the items are available in the system to your current cart
                    if (itemsAvailable == orderArray.length) {
                        output.printCart(currentCart);
                        System.out.println("Thank you for buying from GameCart");
                        System.out.println("type 'commands' for list of available command");
                    } else {
                        System.out.println("Item/s that you are ordering is not available in our system");
                        System.out.println("You can check the available items using 'available' command");
                        System.out.println("And re-type 'order' to re-order");

                    }
                    break;


                case "available":
                    //list available game
                    //list available bundle
                    System.out.println("=================");
                    System.out.println("Available game/s:");
                    System.out.println("=================");
                    output.printAvailableGame(available);
                    System.out.println("=================");
                    System.out.println("Available bundle/s:");
                    System.out.println("=================");
                    output.printAvailableBundle(available);
                    System.out.println("type 'commands' for list of available command");
                    break;

                case "search":
                    // lists available genres
                    System.out.println("searching for item/s in the system by genre:");
                    System.out.println("this is the list available genre in our system:");
                    for(String genre : available.getGenreListAv()){
                        System.out.println("    " + genre);
                    }
                    System.out.println();
                    System.out.println("example for searching by genre:");
                    System.out.println("FPS\n");
                    String genreSearch = in.nextLine();
                    if(available.getGenreListAv().contains(genreSearch)){
                        output.printSearchItemByGenre(available,genreSearch);
                        System.out.println("type 'commands' for list of available command");
                    }
                    else{
                        System.out.println("genre was not found in our system, please search for another genre.");
                        System.out.println("by retyping 'search'\n");
                    }
                    break;

                case "exit":
                    still = false;
                    break;

                default:
                    System.out.println("'" + input + "' is not a registered command");
                    System.out.println("type 'commands' for list of available command");
            }
        }


    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public String nextLine() throws IOException {
            return reader.readLine();
        }
    }
}

