package Game;

import java.util.ArrayList;

public class Game {

    private static int counter=1;
    private String id;
    private String title;
    private int price;
    ArrayList<String> genre = new ArrayList<String>();

    public Game(String title) {
        this.title = title;
        this.id = "G"+ Integer.toString(counter++);
    }

    public Game(String title, int price) {
        this.title = title;
        this.price = price;
        this.id = "G"+ Integer.toString(counter++);
    }

    public Game(String title, int price, ArrayList<String> genre){
        this.title = title;
        this.price = price;
        this.genre = genre;
        this.id = "G"+ Integer.toString(counter++);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void addGenre(String genre) {
        if(!this.genre.contains(genre)){
            this.genre.add(genre);
        }
    }

    public void removeGenre(String genre) {
        if(this.genre.contains(genre)){
            this.genre.remove(genre);
        }
    }

    public ArrayList<String> getGenre() {
        return genre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
