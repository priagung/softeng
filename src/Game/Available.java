package Game;

import java.util.ArrayList;

public class Available {
    private ArrayList<Game> gameListAv;
    private ArrayList<Bundle> bundleListAv;
    private ArrayList<String> genreListAv;

    public Available(){
        this.gameListAv = new ArrayList<Game>();
        this.bundleListAv = new ArrayList<Bundle>();
        this.genreListAv = new ArrayList<String>();
    }

    public ArrayList<Bundle> getBundleListAv() {
        return bundleListAv;
    }

    public ArrayList<Game> getGameListAv() {
        return gameListAv;
    }

    public ArrayList<String> getGenreListAv() {
        return genreListAv;
    }

    public void addGameAv(Game game) {
        if(!this.gameListAv.contains(game)){
            this.gameListAv.add(game);
        }
    }

    public void addBundleAv(Bundle bundle) {
        if(!this.bundleListAv.contains(bundle)){
            this.bundleListAv.add(bundle);
        }
    }

    public void addGenreAv(String genre){
        if(!this.genreListAv.contains(genre)){
            this.genreListAv.add(genre);
        }
    }
}
