package Game;

import java.util.ArrayList;

public class Bundle {

    private static int counter=1;
    private String id;
    private String bundleTitle;
    private int bundlePrice;
    private ArrayList<Game> gameList;
    private long normalPrice;

    public Bundle(String bundleTitle, int bundlePrice) {
        this.id = "B" + Integer.toString(counter++);
        this.bundleTitle = bundleTitle;
        this.bundlePrice = bundlePrice;
        this.gameList = new ArrayList<>();
        this.normalPrice = 0;
    }

    public String getBundleTitle() {
        return bundleTitle;
    }

    public int getBundlePrice() {
        return bundlePrice;
    }

    public ArrayList<Game> getGameList() {
        return gameList;
    }

    public void addGame(Game game) {
        this.gameList.add(game);
        this.normalPrice += game.getPrice();
    }

    public String getId() {
        return id;
    }

    public long getNormalPrice() {
        return normalPrice;
    }
}

