package Game;

import java.util.ArrayList;

public class Cart {

    private ArrayList<Game> gameCart = new ArrayList<>();
    private ArrayList<Bundle> bundleCart = new ArrayList<>();
    private long totalPrice;
    private int itemAmount;

    public Cart(){
        this.totalPrice = 0;
        this.itemAmount = 0;
    }

    public void addGameToCart(Game game, int gameAmount) {
        for (int i = 0; i < gameAmount; i++) {
            this.gameCart.add(game);
            this.totalPrice += game.getPrice();
            this.itemAmount++;
        }
    }

    public void addBundleToCart(Bundle bundle, int bundleAmount){
        for (int i = 0; i < bundleAmount; i++) {
            this.bundleCart.add(bundle);
            this.totalPrice += bundle.getBundlePrice();
            this.itemAmount++;
        }
    }

    public int getItemAmount() {
        return itemAmount;
    }

    public ArrayList<Bundle> getBundleCart() {
        return bundleCart;
    }

    public ArrayList<Game> getGameCart() {
        return gameCart;
    }

    public long getTotalPrice() {
        return totalPrice;
    }
}
